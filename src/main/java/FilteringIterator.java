import java.util.Iterator;


public class FilteringIterator implements Iterator<Object> {
    private final Iterator<Object> objectIterator;
    private final IObjectTest iObjectTest;
    private Object next;

    public FilteringIterator(Iterator<Object> objectIterator, IObjectTest iObjectTest) {
        this.objectIterator = objectIterator;
        this.iObjectTest = iObjectTest;
    }

    public boolean hasNext() {
        while (next == null && objectIterator.hasNext()) {
            next = objectIterator.next();
            if (iObjectTest.test(next))
                return true;
            next = null;
        }
        return next != null;
    }

    public Object next() {
        if (next == null)
            hasNext();
        try {
            return next;
        } finally {
            next = null;
            hasNext();
        }
    }

    public void remove() {
        objectIterator.remove();
    }

}
