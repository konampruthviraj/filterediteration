import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertFalse;
import static org.testng.AssertJUnit.assertEquals;


public class FilteringIteratorTest {
    private IObjectTest objectIterator;

    @Test
    public void testOne() {
        List<Object> inputStrings = Arrays.asList("Zebra", "Camel", "Lion", "Tiger", "Buffalo", "Jaguar");
        objectIterator = o -> Objects.equals(((String)o).length(), 5);
        Iterator<Object> results = new FilteringIterator(inputStrings.iterator(), objectIterator);

        assertEquals("Zebra", results.next());
        assertEquals("Camel", results.next());
        assertEquals("Tiger", results.next());
        assertFalse(results.hasNext());
    }

}
